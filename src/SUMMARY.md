# Obsah

[Úvod](úvod.md)

- [Zapnutí počítače](kapitoly/zapnutí-počítače.md)
- [Ovládání počítače](kapitoly/ovládání-počítače.md)

- [Práce se soubory](kapitoly/soubory/index.md)
  - [Dokumenty](kapitoly/soubory/dokumenty.md)

- [Internet](kapitoly/internet/index.md)
  - [Webový prohlížeč](kapitoly/internet/webový-prohlížeč.md)
  - [Webové stránky a služby](kapitoly/internet/webové-stránky-a-služby.md)
  - [E-maily](kapitoly/internet/e-maily.md)
<!--
  - [Volání a chat](kapitoly/internet/volání-chat.md)
  - [Bezpečnost na internetu](kapitoly/internet/bezpečnost.md)

- [Počítačová nebezpečí](kapitoly/počítačová-nebezpečí/index.md)
  - [Nevyžádaná pošta](kapitoly/počítačová-nebezpečí/nevyžádaná-pošta.md)
  - [Viry](kapitoly/počítačová-nebezpečí/viry.md)
-->

# Pro zvídavé

- [Z čeho se skládá počítač](kapitoly/pro-zvídavé/hardware/index.md)
  - [Uvnitř počítače](kapitoly/pro-zvídavé/hardware/uvnitř-počítače.md)
  - [Periferní zařízení](kapitoly/pro-zvídavé/hardware/periferní-zařízení.md)
- [Jak fungují programy](kapitoly/pro-zvídavé/software/index.md)
  - [Operační systém](kapitoly/pro-zvídavé/software/operační-systém.md)
  - [Programy](kapitoly/pro-zvídavé/software/programy.md)
  - [Licence](kapitoly/pro-zvídavé/software/licence.md)
- [Klávesové zkratky](kapitoly/pro-zvídavé/klávesové-zkratky.md)
- [Velikost souborů](kapitoly/pro-zvídavé/soubory/velikost-souborů.md)
- [Tabulky](kapitoly/pro-zvídavé/soubory/tabulky.md)
- [Prezentace](kapitoly/pro-zvídavé/soubory/prezentace.md)
- [Internet](kapitoly/pro-zvídavé/internet/index.md)
  - [Servery](kapitoly/pro-zvídavé/internet/servery.md)
  - [Další webové stránky](kapitoly/pro-zvídavé/internet/další-webové-stránky-a-služby.md)
  - [Soukromí](kapitoly/pro-zvídavé/internet/soukromí-a-ochrana-údajů.md)
- [Další materiály pro samostudium](kapitoly/pro-zvídavé/další-materiály.md)
