# <i class="fa fa-power-off" aria-hidden="true"></i> Zapnutí počítače

Pokud už máš zapínání počítače zvládnuté, bude takhle kapitola jen opakování a můžeš [přeskočit dolů na její konec](#počítač-mám-zapnutý-co-dál).

<!-- toc -->

## Jak zapnout stolní počítač
_Stolní počítač_ je krabice buď přímo na stole, nebo schovaná pod stolem, aby nezabírala tolik místa, a k ní patří _monitor_ 🖥️, _myš_ 🖱️ a _klávesnice_ ⌨️. Takovýhle počítač zapneš zmáčknutím tlačítka na té krabici, většinou toho největšího a se symbolem <i class="fa fa-power-off" aria-hidden="true"></i> nebo nápisem &bdquo;POWER&ldquo;.

Po zmáčknutí správného tlačítka se počítač spustí, začne trochu hučet a některé počítače i pípnou. Pokud se na monitoru nezačne nic zobrazovat, může být potřeba podobným tlačítkem zapnout i ten.

## Jak zapnout přenosný počítač
Přenosný počítač, _notebook_ či _laptop_ je počítač, který vypadá tak trochu jako velká pootočená kniha. Je přímo na stole a většinou k němu už nic dalšího nepatří, protože má _monitor_ 🖥️ i _klávesnici_ ⌨️ přímo v sobě a místo _myši_ 🖱️ má obdélníkovou plošku, po které se jezdí prstem a které se říká _touchpad_ (angl., čti &bdquo;tačped&ldquo;).

Notebook zapneš taky podobným tlačítkem, které bývá v rohu vedle klávesnice.

<figure>
  <a title="Michal Stanke, CC BY-SA 4.0 &lt;https://creativecommons.org/licenses/by-sa/4.0&gt;" href="../assets/images/Lenovo-V330-button.jpg"><img width="256" alt="Lenovo V330 (1)" src="../assets/images/Lenovo-V330-button-256px.jpg"></a>
</figure>

## Počítač mám zapnutý, co dál
Podařilo se ti počítač zapnout, monitor svítí a něco ukazuje? Pokračuj na další kapitolu kde se dozvíš, jak ho [začít ovládat](ovládání-počítače.md), nebo si před tím ještě přečti něco víc o tom, [z čeho je počítač složený a jak vypadá uvnitř](pro-zvídavé/hardware/index.md).
