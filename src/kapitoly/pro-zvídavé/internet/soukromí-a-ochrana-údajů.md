# Ochrana soukromí na internetu

Při navštěvování webových stránek se nejen ty dozvíš něco zajímavého z obsahu, který je na stránce napsaný, ale stránky a servery se mohou dozvědět také něco o tobě. Kromě zajištění bezpečnosti na webu se taky často mluví o zachování soukromí. Proč?

<!-- toc -->

## Cookies
Cookies (angl. sušenky) slouží k tomu, aby webová stránka poznala, kdo jsme. Udržuje informaci o naší identitě, např. že jsme přihlášení apod. Bohužel je někteří zneužívají i ke sledování všeho, co na webu děláme a co si čteme. S cookies se samozřejmě potkal i Kuba.

<figure>
  <a title="Datová Lhota: Kubova talkshow: Co o nás vědí na internetu — iVysílání — Česká televize" href="https://www.ceskatelevize.cz/ivysilani/11933175266-datova-lhota/bonus/39003-kubova-talkshow-co-o-nas-vedi-na-internetu" target="_blank"><img width="512" alt="Datová Lhota: Kubova talkshow: Co o nás vědí na internetu — iVysílání — Česká televize" src="../../../assets/images/datova-lhota-cookies.jpg"></a>
  <figcaption><small>
    <a href="https://decko.ceskatelevize.cz/datova-lhota" target="_blank">Datová Lhota</a> — Déčko — Česká televize
  </small></figcaption>
</figure>

## Sledovací prvky
Cookies nejsou jediným způsobem jak &bdquo;špehovat&ldquo; lidi na internetu. Podobných technik pro sledování zobrazených reklam nebo tvých zájmů existuje více a jsou často velmi vynalézavé a je složitější se jim vyhnout. Je to částečně proto, že stránky svůj obsah nabízí často zadarmo.

## Reklama
Provoz webových stránek není zadarmo. Webové stránky, jejichž obsah je zdarma a zároveň nic neprodávají, většinou vydělávají na svůj provoz zobrazováním reklamy. Aby dostaly za reklamu zaplaceno, nestačí ji většinou jen zobrazit, ale musíš na ni jako návštěvník taky kliknout, a ještě lépe si pak reklamou nabízenou věc koupit. Proto se všichni snaží zobrazovat _cílenou reklamu_ na věci, které tě zajímají, a k tomu se hodí vědět, co a kde na webu prohlížíš.

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/LWPUG55uuAE" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
