# 📖 Další materiály pro samostudium

Výborně, jsi na konci celé příručky. Pokud ti tu něco chybí, určitě to časem doplním třeba na základě těchto stránek a videí. Mezitím můžeš studovat napřed a pustit si je samostatně:
- [Datová Lhota](https://decko.ceskatelevize.cz/datova-lhota)
- [Alenka v říši GIFů](https://decko.ceskatelevize.cz/alenka-v-risi-gifu)
- [Nebojte se Internetu](https://www.nebojteseinternetu.cz/)
- [Jak na Internet](https://www.jaknainternet.cz/)
- [NÚKIB](https://osveta.nukib.cz/)
