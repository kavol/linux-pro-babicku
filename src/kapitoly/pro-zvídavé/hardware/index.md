# Hardware - z čeho se skládá počítač

Hardware (&bdquo;hard&ldquo; angl. tvrdý) je fyzické technické vybavení počítače. To znamená všechno, na co si můžeš rukou sáhnout nebo &bdquo;do toho kopnout&ldquo;, když počítač nefunguje podle očekávání.

Počítačů a jim podobných zařízení je více druhů podle použití i velikosti. V domácnosti se dneska nejvíc potkáš se stolním počítačem, notebookem nebo tabletem.

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/Mrt-PXIOxek" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Když teď víš, jak počítače vypadají zvenku, pojď se jim podívat i na součástky.

- [Co je uvnitř počítače](uvnitř-počítače.md)
- [Periferní zařízení](periferní-zařízení.md)
