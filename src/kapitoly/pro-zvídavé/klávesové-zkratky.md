# Klávesové zkratky

Klávesové zkratky se používají stisknutím více kláves najednou. Můžeš je zmáčknout všechny naráz nebo jednu po druhé, ale je důležité, aby byly alespoň na chvíli všechny stisknuté společně.

<figure>
  <a title="Petr Sladek (slady), CC BY-SA 3.0 &lt;http://creativecommons.org/licenses/by-sa/3.0/&gt;, prostřednictvím Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:Qwertz_cz.svg"><img width="512" alt="Qwertz cz" src="../../assets/images/klavesnice-qwertz.png"></a>
  <figcaption><small>
    <a href="https://commons.wikimedia.org/wiki/File:Qwertz_cz.svg">Petr Sladek (slady)</a>, <a href="http://creativecommons.org/licenses/by-sa/3.0/">CC BY-SA 3.0</a>, prostřednictvím Wikimedia Commons
  </small></figcaption>
</figure>

- `Ctrl`+`C` zkopíruje označený text nebo vybrané soubory (kvůli tomu `C` se někdy říká &bdquo;kontrol a cizí&ldquo;),
- `Ctrl`+`V` vloží kopírovaný text nebo soubory (kvůli tomu `C` se někdy říká &bdquo;kontrol a vlastní&ldquo;),
- `Ctrl`+`A` označí celý text dokumentu nebo webové stránky, nebo vybere všechny soubory v otevřené složce,
- `Print Screen` úplně samotná uloží všechno, co se zobrazuje na obrazovce jako obrázek. Ten pak najdeš ve složce &bdquo;Obrázky&ldquo; pojmenovaný podle aktuálního data a času. Na klávesnici je někdy označená i zkráceně, třeba `PrtSc`, a bývá vpravo nahoře nebo nad šipkami.
