# Operační systém

Operační systém je první program, který uvidíš při zapínání počítače. Díky operačnímu systému pak můžeš celý počítač [ovládat](../../ovládání-počítače.md). Zobrazuje na monitoru plochu s ikonami, kurzor myši, se kterým můžeš hýbat, snímá klávesy stisknuté na klávesnici atd. Stará se o ovládání veškerého [hardwaru](../hardware/index.md) a připojených [zařízení](../hardware/periferní-zařízení.md), ale taky spouští další [programy](programy.md).

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/2CbmEn9XQZM" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Nejznámější operační systémy pro domácí počítače jsou:
- [<i class="fa fa-windows" aria-hidden="true"></i> Windows](#windows)
- [<i class="fa fa-linux" aria-hidden="true"></i> Linux](#linux)
- [<i class="fa fa-apple" aria-hidden="true"></i> Apple](#macos)

## Windows
Windows (&bdquo;windows&ldquo; angl. okna) jsou operační systém, který byl konkrétně na tvém počítači jako první. Vytváří ho firma Microsoft a nechává si za něj platit.

<figure>
  <a title="Original work: Microsoft; File:Windows 8 logo and wordmark.svg: Multiple editors; see image description page; This work: User:AxG, Public domain, prostřednictvím Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:Windows_logo_and_wordmark_-_2012.svg"><img width="256" alt="Windows logo and wordmark - 2012" src="../../../assets/images/windows-logo.png"></a>
  <figcaption><small>
    <a href="https://commons.wikimedia.org/wiki/File:Windows_logo_and_wordmark_-_2012.svg">Original work: MicrosoftFile:Windows 8 logo and wordmark.svg: Multiple editors; see image description pageThis work: User:AxG</a>, Public domain, prostřednictvím Wikimedia Commons
  </small></figcaption>
</figure>

Do Windows můžeš doinstalovat spoustu programů nebo her (některé zadarmo, jiné za peníze) a i proto jsou na domácích počítačích hodně rozšířené. Na druhou stranu na některých starších počítačích fungují hodně pomalu, a i to byl jeden z důvodů, proč jsme je v tvém počítači nahradili [Linuxem](#linux).

## Linux
Linux je operační systém, který je rozšířený hlavně na serverech (velké a výkonné počítače), ale dá se používat i na domácím počítači. Pracuje na něm spousta firem i jednotlivců, které ho chtějí používat, a kdokoliv další se může přidat. Proto má Linux hodně verzí a variant, kterým se říká _distribuce_, a každá může vypadat a fungovat trochu jinak.

<figure>
  <a title="lewing@isc.tamu.edu Larry Ewing and The GIMP, CC0, prostřednictvím Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:Tux.svg"><img width="128" alt="Tux" src="../../../assets/images/linux-tux.png"></a>
  <figcaption><small>
    <a href="https://commons.wikimedia.org/wiki/File:Tux.svg">lewing@isc.tamu.edu Larry Ewing and The GIMP</a>, CC0, prostřednictvím Wikimedia Commons
  </small></figcaption>
</figure>
<figure>
  <a title="Michal Stanke, CC BY-SA 4.0 &lt;https://creativecommons.org/licenses/by-sa/4.0&gt;, prostřednictvím Wikimedia Commons" href="../../../assets/images/Linux-Mint-20-MATE-desktop.png"><img width="256" alt="Linux-Mint-MATE-20-desktop" src="../../../assets/images/Linux-Mint-20-MATE-desktop-512px.png"></a>
  <figcaption><small>
    <a href="https://commons.wikimedia.org/wiki/File:Linux-Mint-MATE-20-desktop.png">Michal Stanke</a>, <a href="https://creativecommons.org/licenses/by-sa/4.0">CC BY-SA 4.0</a>, prostřednictvím Wikimedia Commons
  </small></figcaption>
</figure>
<figure>
  <a title="Giorgosarv18 (diskuse · příspěvky), GPL &lt;http://www.gnu.org/licenses/gpl.html&gt;, prostřednictvím Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:Desktop_Ubuntu_20.04.png"><img width="256" alt="Desktop Ubuntu 20.04" src="../../../assets/images/linux-ubuntu-screenshot.png"></a>
  <figcaption><small>
    <a href="https://commons.wikimedia.org/wiki/File:Desktop_Ubuntu_20.04.png">Giorgosarv18 (diskuse · příspěvky)</a>, <a href="http://www.gnu.org/licenses/gpl.html">GPL</a>, prostřednictvím Wikimedia Commons
  </small></figcaption>
</figure>

Programů a her, které fungují s Linuxem, je asi trochu méně než u [Windows](#windows). Většinou jsou ale dostupné pod [svobodnou licencí](licence.md#svobodná-licence) a nemusíš za ně ani nic platit. Díky více verzím Linuxu tak třeba není tak těžké najít některou, která funguje docela svižně i na starších nebo levných a málo výkonných počítačích.

## macOS
macOS je operační systém, který prodává firma Apple (&bdquo;apple&ldquo; angl. jablko) společně se svými počítači. Většinou jsou to notebooky ve stříbrné barvě a hodně se objevují ve filmech. Poznáš je podle loga s nakousnutým jablkem.

<figure>
  <a title="Původní dílo:  Rob Janoff, Public domain, prostřednictvím Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:Apple_logo_black.svg"><img width="128" alt="Apple logo black" src="../../../assets/images/apple-logo.png"></a>
  <figcaption><small>
    <a href="https://commons.wikimedia.org/wiki/File:Apple_logo_black.svg">Původní dílo:  Rob Janoff</a>, Public domain, prostřednictvím Wikimedia Commons
  </small></figcaption>
</figure>
