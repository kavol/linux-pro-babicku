# Licence

Jako autor programu se musíš rozhodnout, pod jakou licencí (za jakých podmínek) ho mohou ostatní získat, používat, upravovat, nebo třeba i dál prodávat.

<!-- toc -->

## Proprietární software
Proprietární je takový program, kdy jeho [zdrojový kód](programy.md#zdrojový-kód) zná jenom jeho autor a nikdo další ho nemůže číst a tedy ani zkontrolovat, co přesně program podle svého zdrojového kódu na počítači dělá. Pro tebe jako uživatele může být takový program pořád zadarmo, ale často je potřeba si ho koupit.

## Svobodná licence
Programy dostupné pod svobodnou licencí se označují jako svobodné nebo open-source (&bdquo;open&ldquo; angl. otevřený, &bdquo;source&ldquo; angl. zdroj). [Zdrojový kód](programy.md#zdrojový-kód) takových programů je veřejně dostupný a za podmínek určených autorem může kdokoliv ten kód vzít, zkontrolovat a případně i upravovat a použít pro vlastní program. Příkladem je [Linux](operační-systém.md#linux).

Protože je zdrojový kód těchto programů většinou veřejně na internetu, můžeš je používat zadarmo. Nemusí to tak být vždy, ale většinou opravdu zadarmo jsou. Jejich autoři vydělávají na souvisejících službách, třeba pro firmy, které program používají při svém podnikání. Někteří na programu nevydělávají vůbec a vytváří ho proto, aby ho mohli sami používat.

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/37RpaBM8y4A" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
