# 📃 Webové stránky a služby

Druhů webových stránek je celá řada, prakticky cokoliv tě napadne od novin, přes televizi, obchody po osobní stránky. Vyjmenovávat všechny je prakticky nemožné a nikdo by to nedokázal, proto zmíním vždy jen nějaké příklady.

<!-- toc -->

## Vyhledávače
Webové vyhledávače slouží k hledání dalších stránek podle textu, který obsahují, nebo tématu, o kterém se na nich píše. Jsou to takové katalogy zbytku webu.

Většina vyhledávačů vypadá stejně a mají vlastně jen jedno velké políčko, kam napíšeš, co hledáš. Takže třeba &bdquo;krmení pro kočku&ldquo;, &bdquo;předpověď počasí&ldquo;, &bdquo;kurz Eura&ldquo; nebo &bdquo;mapa Prahy&ldquo;. Nejpoužívanější vyhledávač je Google (čti &bdquo;gůgl&ldquo;) na adrese [_google.com_](https://www.google.com/).

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/tS0S549aIc4" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Pokud chceš hledat jenom na konkrétní stránce, nemusíš hned otevírat Google. Většina stránek má podobné vyhledávací políčko vlastní, které ti poslouží podobně, ale hledá jenom na té stránce.

## Zpravodajské servery
Zpravodajské weby jsou takové internetové noviny. Jenom nemusíš čekat na jejich vytištění ani si je chodit kupovat. Na hlavní stránce takového webu hned uvidíš nejnovější nebo nejčtenější zprávy z Čech i ze světa, a články si může vytřídit i podle témat jako jsou finance, sport apod.

Českých zpravodajských serverů je opravdu hodně, např. [České noviny](https://www.ceskenoviny.cz/), [Aktuálně](https://www.aktualne.cz/), [Seznam Zprávy](https://www.seznamzpravy.cz/) nebo [web ČT24](https://ct24.ceskatelevize.cz/). Stejně jako v novinách, některé články na těchto webech mohou být reportáže a některé jen názory a komentáře členů redakce.

## E-shopy
E-shopy (čti &bdquo;é šop&ldquo; nebo angl. &bdquo;í šop&ldquo;) jsou internetové obchody. Přes internet se v nějakém e-shopu dá koupit téměř cokoliv. Jeden e-shop nemusí ani nabízet zboží jenom jednoho typu, třeba elektroniku, ale může ji kombinovat i s drogérií nebo hračkami. Výhodou nakupování na internetu je také možnost pomocí srovnávačů (speciálních vyhledávačů pro e-shopy) najít ten nejlevnější nebo ten, se kterým mají zákazíci dobré zkušenosti.

Během pandemie COVIDu získaly velmi na popularitě internetové obchody s potravinami, které ti je dokonce do několika hodin nebo na druhý den dovezou až domů.

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/hotEc1mozU4" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Další
Těch druhů stránek je mnohem víc. Až se seznámíš s těmito, další najdeš v kapitole [&bdquo;pro zvídavé&ldquo;](../pro-zvídavé/internet/další-webové-stránky-a-služby.md). Pokud myslíš, že ti to zatím stačí, můžeš se tam podívat později a teď si pro změnu přečíst něco o [e-mailech](e-maily.md).
